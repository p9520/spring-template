package com.example.demo.web;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class MyBestController {


    @GetMapping("/")
    String home() {
        return "Spring is here!";
    }
}
